// set the dimensions and margins of the graph
var width = 800
var height = 800

var svg = null;
var bubbles = null;
var nodes = [];

// append the svg object to the body of the page
var svg = d3.select("#vis")
  .append("svg")
  .attr("class","map")
  .attr("width", width)
  .attr("height", height)

transitioned = false;
wasDragged = false;
brewed = false;

// create dummy data -> just one element per circle
var data = [{
  "name": "portable",
  "title":"Gossip Dumpling",
  "id":"HOportable1",
  "tag": ["gossip","sight"],
  "thumb": "data/sight/HOportable_20200206_dumpling_thumb.png",
  "img": ["data/sight/HOportable_20200206_dumpling.jpg"],
  "desc":"Gossip filling from the dumpling session; Radio Slumber One, 23 January 2020",
  "nsize": 7
}, {
  "name": "portable",
  "title":"Behind Radio Slumber",
  "id":"HOportable2",
  "tag": ["gossip","sight"],
  "thumb": "data/sight/HOportable_2020-12-06-at-23.09.46meeting_thumb.png",
  "img": ["data/sight/HOportable_2020-12-06-at-23.09.46meeting.jpg"],
  "desc":"The infinite Drostian process of online meetings; Sudo, portable and Angeliki on Jitsi, 6 December 2020",
  "nsize": 7
}, {
  "name": "portable",
  "title":"Behind Radio Slumber",
  "id":"HOportable3",
  "tag": ["care","sight", "diasporas"],
  "thumb": "data/sight/HOportable_20200212_power-flower-session_thumb.png",
  "img": ["data/sight/HOportable_20200212_power-flower-session.jpg"],
  "desc":"The &#8220;Power Flower &#8221; workshop session led by Mai Ling collective members An Yela (far left) and Virginia Lui (far right); Motel Spatie, 12 February 2020",
  "nsize": 7
}, {
  "name": "portable",
  "title":"Behind Radio Slumber",
  "id":"HOportable4",
  "tag": ["care","sight", "friendship"],
  "thumb": "data/sight/HOportable_20200216_cove01_thumb.png",
  "img": ["data/sight/HOportable_20200216_cove01.jpg"],
  "desc":"The Radio Slumber cove, featuring a painted illustration on cotton muslin by artist Erik Tlaseca; Arnhem, Netherlands, Winter 2020",
  "nsize": 7
}, {
  "name": "portable",
  "title":"Behind Radio Slumber",
  "id":"HOportable5",
  "tag": ["care","sight", "friendship"],
  "thumb": "data/sight/HOportable_20200216_cove02_thumb.png",
  "img": ["data/sight/HOportable_20200216_cove02.jpg"],
  "desc":"The Radio Slumber cove interior view; Arnhem, Netherlands, Winter 2020",
  "nsize": 7
}, {
  "name": "Li Xiaotian",
  "title":"ChaoCo-op Meeting",
  "id":"LIxiaotian1",
  "tag": ["care","sight", "witches"],
  "thumb": "data/sight/LIxiaotian_doorcosy-meeting_thumb.png",
  "img": ["data/sight/LIxiaotian_doorcosy-meeting.jpg"],
  "desc":"苗子 Miao, 丹宝 Dan Bao and 敏 Min of Chao Co-op discuss the production of the Radio Slumber door cosy",
  "nsize": 7
}, {
  "name": "Li Xiaotian",
  "title":"ChaoCo-op Door Cosy Samples",
  "id":"LIxiaotian2",
  "tag": ["gossip","sight", "witches"],
  "thumb": "data/sight/LIxiaotian_doorcosy-samples_thumb.png",
  "img": ["data/sight/LIxiaotian_doorcosy-samples.jpg"],
  "desc":"Baby-testing versions of the Radio Slumber door cosy",
  "nsize": 7
}, {
  "name": "Ursula le Guin",
  "title":"The Carrier Bag Theory of Fiction",
  "id":"ursulaleguin",
  "tag": ["witches","read"],
  "thumb": "data/read/leGUINursula_The_Carrier_Bag_Theory_of_Fiction_thumb.png",
  "pdf": ["data/read/leGUINursula_The_Carrier_Bag_Theory_of_Fiction.pdf"],
  "desc": "<em>I just read this short essay called &#8220;The Carrier Bag Theory of Fiction&#8221; by Ursula K. le Guin. It talks about science fiction as being more than  a myth of  technology in retelling the narrative of the heroic man dominating nature, but rather how it can be understood as the carrier bag, the sling or gourd. In contrast to the murderous logic of the spear big that speaks of conquests, she argues for the container as the first cultural device. Its ability to hold all sorts of small grains and plants so that it may be brought back home, is metaphorically use to reclaim science fiction as a genre in telling mythological stories to one that is closer to a realistic portrayal of the everyday moments of failures, misunderstandings and delusions. ~Sudo</em>",
  "nsize": 7
}, {
      "name": "Hannah Black",
  "title":"Witch-hunt",
  "id":"hannablack",
  "tag": ["witches","read", "gossip", "friendship"],
  "thumb": "data/read/BLACKhannah_Witch-hunt_thumb.png",
  "pdf": ["data/read/BLACKhannah_Witch-hunt.pdf"],
  "desc": "From the <em>afwasdoekje</em> reading groups of Radio Slumber One and Radio Slumber Two, scanned from <em>TANK</em> magazine issue 70",
  "nsize": 7
}, {
  "name": "Matilda Kenttä",
  "title":"Res(t)idency",
  "id":"matildakentta1",
  "tag": ["witches","read", "gossip", "friendship"],
  "thumb": "data/read/KunstvereinAmsterdam_MatildaKenttä_thumb.png",
  "pdf": ["data/read/KunstvereinAmsterdam_MatildaKenttä_Booklet.pdf"],
  "desc": "This booklet was produced to mark the culmination of <em>Making Bread into Dough</em>—Matilda Kenttä’s 2020 Summer Res(t)idency at Kunstverein, Amsterdam. Working away while the institution was on its summer break, Kenttä questioned who is actually afforded the luxury of time off through weaving full-time on site for a period of two months. From weaving’s demarcation as ‘women’s work’ to the loom as a site of shared labour in which gossip productively springs, the four contributions in this publication ruminate on the different threads taken up in Kenttä’s practice. Texts by Amelia Groom, Elina Birkehag, Adelheid Smit and Elaine Wing-Ah Ho. Edited by Yana Foqué and Isabelle Sully, and designed by Marc Hollenstein.",
  "nsize": 7
}, {
  "name": "Matilda Kenttä",
  "title":"A Mouth Has Far More Flesh Than a Butterfly Can Carry",
  "id":"matildakentta2",
  "tag": ["witches","read", "gossip", "friendship"],
  "thumb": "data/read/KENTTÄmatilda_AMouthHasFarMoreFleshThanaButterflyCanCarry_thumb.png",
  "pdf": ["data/read/KENTTÄmatilda_AMouthHasFarMoreFleshThanaButterflyCanCarry.pdf"],
  "desc": "XX",
  "nsize": 7
}, {
  "name": "Hélène Cixous",
  "title":"Laugh of the Medusa",
  "id":"helenecixous",
  "tag": ["witches","read", "gossip", "friendship"],
  "thumb": "data/read/CIXOUShelene_LaughoftheMedusa_thumb.png",
  "pdf": ["data/read/CIXOUShelene_LaughoftheMedusa.pdf"],
  "desc": "&#8220;<em>Now women return from afar, from always: from &#8216;without,&#8217; from the heath where witches are kept alive; from below, from beyond &#8216;culture&#8217;; from their childhood which men have been trying desperately to make them forget, condemning it to &#8216;eternal rest.&#8217; The little girls and their &#8216;ill-mannered&#8217; bodies immured, well-preserved, intact unto themselves, in the mirror. Frigidified. But are they ever seething underneath!</em>&#8221;",
  "nsize": 7
}, {
  "name": "Leela Gandhi",
  "title":"Affective Communities",
  "id":"leelagandhi",
  "tag": ["care","read", "gossip", "friendship"],
  "thumb": "data/read/GANDHIleela_Affective-Communities-anticolonial-thought-and-the-politics-of-friendship_thumb.png",
  "pdf": ["data/read/GANDHIleela_Affective-Communities-anticolonial-thought-and-the-politics-of-friendship.pdf"],
  "desc": "XX",
  "nsize": 7
}, {
  "name": "Sheena Malhotra & Aimee Carrillo Rowe",
  "title":"Silence, Feminism, Power",
  "id":"malhotra-rowe",
  "tag": ["witches","read", "gossip", "friendship"],
  "thumb": "data/read/MALHOTRAsheena,_ROWEaimeeCarrillo-eds_Silence-Feminism-Power_thumb.png",
  "pdf": ["data/read/MALHOTRAsheena,_ROWEaimeeCarrillo-eds_Silence-Feminism-Power.pdf"],
  "desc": "Gossip from Ying Que of Read-in for Radio Slumber Three",
  "nsize": 7
}, {
  "name": "Jamie Heckert",
  "title":"Listening, Caring, Becoming",
  "id":"jamieheckert",
  "tag": ["care","read", "gossip", "friendship"],
  "thumb": "data/read/HECKERTjamie_listening-caring-becoming-anarchism-as-an-ethics-of-direct-relationships_thumb.png",
  "pdf": ["data/read/HECKERTjamie_listening-caring-becoming-anarchism-as-an-ethics-of-direct-relationships.pdf"],
  "desc": "&#8220;<em>My proposal here is an ethics with neither origin nor conclusion, ethics which are continually produced in the present, in being present.</em> [...] <em>They are born of relationships, of relating: directly, intersubjectively and warmly. An intimate process which never ends...</em>&#8221;",
  "nsize": 7
}, {
  "name": "Carol Gilligan",
  "title":"Moral Injury and the Ethic of Care: Reframing the Conversation about Differences",
  "id":"carolgilligan",
  "tag": ["care","read", "friendship"],
  "thumb": "data/read/GILLIGANcarol_Moral-Injury-and-the-Ethic-of-Care_thumb.png",
  "pdf": ["data/read/GILLIGANcarol_Moral-Injury-and-the-Ethic-of-Care.pdf"],
  "desc": "Gossip from Read-in for the <em>afwasdoekje</em> reading group of Radio Slumber Two: &#8220;<em>&#8216;I don’t know,&#8217; girls say; &#8216;I don’t care,&#8216; boys say.</em>&#8221;",
  "nsize": 7
}, {
  "name": "Chao Co-op",
  "title":"Making of the Radio Slumber Door Cosy",
  "id":"chao",
  "tag": ["gossip","watch","witches"],
  "thumb": "data/sight/CHAOcoop_process01_thumb.png",
  "video": ["data/watch/CHAOcoop_process01.mp4"],
  "desc":"丹宝 Dan Bao of the Chao Co-op beginning production samples for the PWSSSFRS Door Cosy",
  "nsize": 7
}, {
  "name": "Mai Ling Kocht Nr. 1",
  "title":"Eight Treasures",
  "id":"youtube",
  "tag": ["diasporas","watch","witches"],
  "thumb": "data/watch/MAIling_mailingkocht01_thumb.png",
  "video": ["https://www.youtube.com/embed/Cdr4ZYlptNg"],
  "desc":"<em>Mai Ling Kocht #1</em> was first performed live in September 2020 at the Migrating Kitchen in Vienna, a multimedia cooking piece to underscore the complexities between food politics and culinary comfort.<br><br>An ASMR audio version of the third edition of the <em>Mai Ling Kocht</em> series has been commissioned for RADIO SLUMBER THREE, also to premiere live mid-February 2021 with the support of kültür gemma! Vienna.",
  "nsize": 7
}, {
  "name": "Angeliki Diakrousi",
  "title":"Gossip Test",
  "id":"Angeliki",
  "tag": ["gossip","sound"],
  "thumb": "data/sound/DIAKROUSI_gossip_test_with_simon_thumb.png",
  "audio": ["data/sound/DIAKROUSI_gossip_test_with_simon.wav"],
  "desc":"Gossip test with simon",
  "nsize": 7
}, {
  "name": "portable & Sudo",
  "title":"Racial Facial",
  "id":"portableSudo",
  "tag": ["care","sound","diasporas"],
  "thumb": "data/sound/HOportable_WUsudo_RacialFacial_thumb.png",
  "audio": ["https://soundcloud.com/portableho/racial-facial"],
  "desc":"A Meditation on Skin and Care and its Interconnecting Tissues; first performed for Radio Slumber Two, 12 February 2020. <em>*For best results, listen while laying down and a partner is applying a soothing facial treatment on you.</em>",
  "nsize": 7
}]




var defs = svg.append('svg:defs');

defs.selectAll(null)
  .data(data)
  .enter()
  .append("svg:pattern")
  .attr('id', function(d) {
    return 'image' + d.id;
  })
  .attr('x', 0)
  .attr('y', 0)
  .attr('width', function(d) {
    return d.nsize;
  })
  .attr('height', function(d) {
    return 4 * d.nsize;
  })
  .append('svg:image')
  .attr('xlink:href', function(d) {
    return d.thumb;
  })
  .attr('x', function(d) {
    return -4 * d.nsize;
  })
  .attr('y', function(d) {
    return -3 * d.nsize;
  })
  .attr('width', function(d) {
    return 26 * d.nsize;
  })
  .attr('height', function(d) {
    return 26 * d.nsize;
  });




// Initialize the circle: all located at the center of the svg area
var node = svg.append("g")
  .selectAll("circle")
  // var node = svg.selectAll('circle')
  .data(data)
  .enter()
  // .append('svg:a')
  // .attr("xlink:href", function(d) {
  //   return d.img;
  // })
  // .attr('target', '_')
  .append("circle")
  .attr("class", function(d) {
    return (d.tag[0]+" "+d.tag[1]+" "+d.tag[2]);
  })
  .attr("id", function(d) {
    return d.id;
  })
  .attr("r", function() {return (0.02*window.innerWidth)})
  .attr("cx", width / 2)
  .attr("cy", height)
  .style("visibility", "hidden")
  .style("fill", function(d) {
    return "url(#image" + d.id + ")";
  })
  .on("click", function(d){popup(d)});
      d3.selectAll("tbody tr").data(data)
        .selectAll("td")
          .data(function(d, i) { return d; }); // d is matrix[i]




// A function that hides the bubbles that don't belong to relevent tag BUTTON
var changetag = function(tag) {
  if (brewed == true) {
    d3.selectAll("circle").style("visibility", "visible")
    d3.selectAll("circle").filter(function() {
        return !this.classList.contains(tag)
      })
      .style("visibility", "hidden");
  }
}


var showAll = function() {
  node
    .style("visibility", "visible")
}


// Features of the forces applied to the nodes:
var simulation = d3.forceSimulation()
  .force("center", d3.forceCenter().x(width / 2).y(height / 2.5)) // Attraction to the center of the svg area
  .force("charge", d3.forceManyBody().strength(0.1)) // Nodes are attracted one each other of value is > 0
  .force("collide", d3.forceCollide().strength(.01).radius(0.03*window.innerWidth));
// Force that avoids circle overlapping

// Apply these forces to the nodes and update their positions.
// Once the force algorithm is happy with positions ('alpha' value is low enough), simulations will stop.
// Animation: bubbles first appear and animation

// add reset
function brew() {
  simulation.restart()
    .nodes(data)
    .on("tick", function(d) {
      node
        .style("visibility", "visible")
        .transition()
        .delay(100) // New Position
        // .duration(150)
        .attr("cx", function(d) {
          return d.x;
        })
        .attr("cy", function(d) {
          return d.y;
        })
        $(".tag").fadeIn()
				console.log("around")
      brewed = true;
    })
};



// What happens when a circle is dragged?
// function dragstarted(d) {
//   if (!d3.event.active) simulation.alphaTarget(.03).restart();
//   d.fx = d.x;
//   d.fy = d.y;
// }
//
// function dragged(d) {
//   d.fx = d3.event.x;
//   d.fy = d3.event.y;
//   wasDragged = true;
// }
//
// function dragended(d) {
//   if (!d3.event.active) simulation.alphaTarget(.03);
//   d.fx = null;
//   d.fy = null;
//   wasDragged = false;
// }



// example

// d3.select("body")
//   .append("div")
//   .each(function() {
//     d3.select(this).append("button").html("button")
//     d3.select(this).append("div")
//       .each(function() {
//          d3.select(this).append("span").text("span")
//          d3.select(this).append("span").text("span")
//       })
//   })

var div = d3.select("body").append("div");
var span= d3.select("body").append("span").on("click", function(){document.getElementById("modal").style.display = "none";});


function popup(d) {
   if (d.audio !== undefined){
      div.transition().duration(100);
      div.html("<div class='popup'><span onclick=this.parentElement.style.display='none' class='topleft'>&times</span><div class='content'><audio controls><source src='"+d.audio+"' type='audio/mpeg'></audio><br><div>"+d.desc+"</div></div></div>");
    } else if (d.pdf) {
      div.transition().duration(100);
      div.html("<div class='popup'><span onclick=this.parentElement.style.display='none' class='topleft'>&times</span><div class='content'><iframe src='"+d.pdf+"' width='100%' height='500px'></iframe><br><div>"+d.desc+"</div></div></div>");
    } else if (d.video) {
      div.transition().duration(100);
      div.html("<div class='popup'><span onclick=this.parentElement.style.display='none' class='topleft'>&times</span><div class='content'><iframe src='"+d.video+"' width='100%'></iframe><br><div>"+d.desc+"</div></div></div>");
    } else {
      if (d.img[1]){
        div.transition().duration(100);
        div.html("<div class='popup'><span onclick=this.parentElement.style.display='none' class='topleft'>&times</span><div class='content'><img src='"+d.img[0]+"'><br><br><img src='"+d.img[1]+"'><br><br><div>"+d.desc+"</div></div></div>"); } else {
          div.transition().duration(100);
          div.html("<div class='popup'><span onclick=this.parentElement.style.display='none' class='topleft'>&times</span><div class='content'><img src='"+d.img[0]+"'<br><br><div>"+d.desc+"</div></div></div>");
        }

      };
}
