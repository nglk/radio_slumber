/* bubbleChart creation function. Returns a function that will
 * instantiate a new bubble chart given a DOM element to display
 * it in and a dataset to visualize.
 *
 * Organization and style inspired by:
 * https://bost.ocks.org/mike/chart/
 *
 */
function bubbleChart() {
  // Constants for sizing
  var width = 1040;
  var height = 700;

  // tooltip for mouseover functionality
  var tooltip = floatingTooltip('gates_tooltip', 240);

  // Locations to move bubbles towards, depending
  // on which view mode is selected.
  var center = {
    x: width / 2,
    y: height / 2
  };

  var tagCenters = {
    2008: {
      x: width / 3,
      y: height / 2
    },
    2009: {
      x: width / 2,
      y: height / 2
    },
    2010: {
      x: 2 * width / 3,
      y: height / 2
    }
  };

  // X locations of the tag titles.
  var tagsTitleX = {
    2008: 160,
    2009: width / 2,
    2010: width - 160
  };

  // @v4 strength to apply to the position forces
  var forceStrength = 0.5;

  // These will be set in create_nodes and create_vis
  var svg = null;
  var bubbles = null;
  var nodes = [];

  // Charge function that is called for each node.
  // As part of the ManyBody force.
  // This is what creates the repulsion between nodes.
  //
  // Charge is proportional to the diameter of the
  // circle (which is stored in the radius attribute
  // of the circle's associated data.
  //
  // This is done to allow for accurate collision
  // detection with nodes of different sizes.
  //
  // Charge is negative because we want nodes to repel.
  // @v4 Before the charge was a stand-alone attribute
  //  of the force layout. Now we can use it as a separate force!
  function charge(d) {
    return -Math.pow(d.radius, 1.1) * forceStrength;
  }

  // Here we create a force layout and
  // @v4 We create a force simulation now and
  //  add forces to it.
  var simulation = d3.forceSimulation()
    .velocityDecay(0.4)
    .force('x', d3.forceX().strength(forceStrength).x(center.x))
    .force('y', d3.forceY().strength(forceStrength).y(center.y))
    .force('charge', d3.forceManyBody().strength(charge))
    .on('tick', ticked);

  // @v4 Force starts up automatically,
  //  which we don't want as there aren't any nodes yet.
  simulation.stop();

  // Nice looking colors - no reason to buck the trend
  // @v4 scales now have a flattened naming scheme
  var fillColor = d3.scaleOrdinal()
    .domain(['low', 'medium', 'high'])
    .range(['#d84b2a', '#beccae', '#7aa25c']);


  /*
   * This data manipulation function takes the raw data from
   * the CSV file and converts it into an array of node objects.
   * Each node will store data and visualization values to visualize
   * a bubble.
   *
   * rawData is expected to be an array of data objects, read in from
   * one of d3's loading functions like d3.csv.
   *
   * This function returns the new node array, with a node in that
   * array for each element in the rawData input.
   */
  function createNodes(rawData) {
    // Use the max total_amount in the data as the max in the scale's domain
    // note we have to ensure the total_amount is a number.
    var maxAmount = d3.max(rawData, function(d) {
      return +d.total_amount;
    });

    // Sizes bubbles based on area.
    // @v4: new flattened scale names.
    var radiusScale = d3.scalePow()
      .exponent(0.5)
      .range([30, 30])
      .domain([0, maxAmount]);


    // Use map() to convert raw data into node data.
    // Checkout http://learnjsdata.com/ for more on
    // working with data.
    var myNodes = rawData.map(function(d) {
      return {
        id: d.id,
        radius: radiusScale(+d.total_amount),
        value: +d.total_amount,
        name: d.grant_title,
        org: d.organization,
        group: d.group,
        tag: d.tag,
        img: d.img,
        x: Math.random() * 500,
        y: Math.random() * 500
      };
    });

    // sort them to prevent occlusion of smaller nodes.
    myNodes.sort(function(a, b) {
      return b.value - a.value;
    });

    return myNodes;
  }

  /*
   * Main entry point to the bubble chart. This function is returned
   * by the parent closure. It prepares the rawData for visualization
   * and adds an svg element to the provided selector and starts the
   * visualization creation process.
   *
   * selector is expected to be a DOM element or CSS selector that
   * points to the parent element of the bubble chart. Inside this
   * element, the code will add the SVG continer for the visualization.
   *
   * rawData is expected to be an array of data objects as provided by
   * a d3 loading function like d3.csv.
   */
  var chart = function chart(selector, rawData) {
      // convert raw data into nodes data
      nodes = createNodes(rawData);

      // Create a SVG element inside the provided selector
      // with desired size.
      svg = d3.select(selector)
        .append('svg')
        .attr('width', width)
        .attr('height', height);

      // Bind nodes data to what will become DOM elements to represent them.
      bubbles = svg.selectAll('.bubble')
        .data(nodes, function(d) {
          return d.id;
        });

      // Create new circle elements each with class `bubble`.
      // There will be one circle.bubble for each object in the nodes array.
      // Initially, their radius (r attribute) will be 0.
      // @v4 Selections are immutable, so lets capture the
      //  enter selection to apply our transtition to below.


        // bubbles.append("svg:pattern")
        //     .attr("id", "grump_avatar")
        //     .attr("width", "200")
        //     .attr("height", "200")
        //     .attr("patternUnits", "userSpaceOnUse")
        //     .append("svg:image")
        //     .attr("xlink:href", function(d){return d.img})
        //     .attr("width", "100")
        //     .attr("height", "100")
        //     .attr("x", 0)
        //     .attr("y", 0);

  //       defs = svg;
  // defs
       bubbles.append('svg:pattern')
       .attr('id', 'image')
       .attr('x', 0)
       .attr('y', 0)
       .attr('width', 9)
       .attr('height', 39)
       .append('svg:image')
       .attr('xlink:href', 'https://lh3.googleusercontent.com/-epZdTUpfes8/AAAAAAAAAAI/AAAAAAAAEuA/eYM1LLFGA2s/photo.jpg')
       .attr('x', -8)
       .attr('y', -8)
       .attr('width', 52)
       .attr('height', 56)
  	 .attr('stroke-width','1px')
  	 .attr('stroke','#d8d8d8');


      var bubblesE = bubbles.enter().append('circle')
        .classed('bubble', true)
        .attr('r', 0)
        // .attr('fill', function (d) { return fillColor(d.group); })
        // .attr('fill', 'transparent' )
        .style("fill", "url(#image)")
        .attr('stroke', function(d) {
          return d3.rgb(fillColor(d.group)).darker();
        })
        .attr('stroke-width', 2)
        .on('mouseover', showDetail)
        .on('mouseout', hideDetail);


          // Enter any new nodes.
          // var bubblesE = bubbles.enter().append("svg:g")
          //     .classed('bubble', true)
          //     .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
          //     .on('mouseover', showDetail)
          //     .on('mouseout', hideDetail);




          // Append images
          // var bubblesE = bubbles.enter().append("svg:image")
          //        .classed('bubble', true)
          //       .attr("xlink:href",  function(d) { return d.img;})
          //       .attr("x", function(d) { return -25;})
          //       .attr("y", function(d) { return -25;})
          //       .attr("height", 100)
          //       .attr("width", 100)
          //       .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
          //       .on('mouseover', showDetail)
          //       .on('mouseout', hideDetail);

          // // Append a circle
          // bubblesE.append("svg:circle")
          //     .attr("r", 0)
          //     .style("fill", "#eee")
          //     .attr('stroke', function (d) { return d3.rgb(fillColor(d.group)).darker(); })
          //     .attr('stroke-width', 2)
          //     .on('mouseover', showDetail)
          //     .on('mouseout', hideDetail);


          // @v4 Merge the original empty selection and the enter selection
          bubbles = bubbles.merge(bubblesE);

          // Fancy transition to make bubbles appear, ending with the
          // correct radius
          bubbles.transition()
            .duration(2000)
            .attr('r', function(d) {
              return d.radius;
            });

          // Set the simulation's nodes to our newly created nodes array.
          // @v4 Once we set the nodes, the simulation will start running automatically!
          simulation.nodes(nodes);

          // Set initial layout to single group.
          groupBubbles();
        };

        /*
         * Callback function that is called after every tick of the
         * force simulation.
         * Here we do the acutal repositioning of the SVG circles
         * based on the current x and y values of their bound node data.
         * These x and y values are modified by the force simulation.
         */
        function ticked() {
          bubbles
            .attr('cx', function(d) {
              return d.x;
            })
            .attr('cy', function(d) {
              return d.y;
            });
        }

        /*
         * Provides a x value for each node to be used with the split by tag
         * x force.
         */
        function nodetagPos(d) {
          return tagCenters[d.tag].x;
        }


        /*
         * Sets visualization in "single group mode".
         * The tag labels are hidden and the force layout
         * tick function is set to move all nodes to the
         * center of the visualization.
         */
        function groupBubbles() {
          hidetagTitles();

          // @v4 Reset the 'x' force to draw the bubbles to the center.
          simulation.force('x', d3.forceX().strength(forceStrength).x(center.x));

          // @v4 We can reset the alpha value and restart the simulation
          simulation.alpha(1).restart();
        }


        /*
         * Sets visualization in "split by tag mode".
         * The tag labels are shown and the force layout
         * tick function is set to move nodes to the
         * tagCenter of their data's tag.
         */
        function splitBubbles() {
          showtagTitles();

          // @v4 Reset the 'x' force to draw the bubbles to their tag centers
          simulation.force('x', d3.forceX().strength(forceStrength).x(nodetagPos));

          // @v4 We can reset the alpha value and restart the simulation
          simulation.alpha(1).restart();
        }

        /*
         * Hides tag title displays.
         */
        function hidetagTitles() {
          svg.selectAll('.tag').remove();
        }

        /*
         * Shows tag title displays.
         */
        function showtagTitles() {
          // Another way to do this would be to create
          // the tag texts once and then just hide them.
          var tagsData = d3.keys(tagsTitleX);
          var tags = svg.selectAll('.tag')
            .data(tagsData);

          tags.enter().append('text')
            .attr('class', 'tag')
            .attr('x', function(d) {
              return tagsTitleX[d];
            })
            .attr('y', 40)
            .attr('text-anchor', 'middle')
            .text(function(d) {
              return d;
            });
        }


        /*
         * Function called on mouseover to display the
         * details of a bubble in the tooltip.
         */
        function showDetail(d) {
          // change outline to indicate hover state.
          d3.select(this).attr('stroke', 'black');

          // content shown in the tooltip
          var content = '<span class="name">Title: </span><span class="value">' +
            d.name +
            '</span><br/>' +
            '<span class="name">Amount: </span><span class="value">$' +
            addCommas(d.value) +
            '</span><br/>' +
            '<span class="name">tag: </span><span class="value">' +
            d.tag +
            '</span>';

          tooltip.showTooltip(content, d3.event);
        }

        /*
         * Hides tooltip
         */
        function hideDetail(d) {
          // reset outline
          d3.select(this)
            .attr('stroke', d3.rgb(fillColor(d.group)).darker());

          tooltip.hideTooltip();
        }

        /*
         * Externally accessible function (this is attached to the
         * returned chart function). Allows the visualization to toggle
         * between "single group" and "split by tag" modes.
         *
         * displayName is expected to be a string and either 'tag' or 'all'.
         */
        chart.toggleDisplay = function(displayName) {
          if (displayName === 'tag') {
            splitBubbles();
          } else {
            groupBubbles();
          }
        };


        // return the chart function from closure.
        return chart;
      }

      /*
       * Below is the initialization code as well as some helper functions
       * to create a new bubble chart instance, load the data, and display it.
       */

      var myBubbleChart = bubbleChart();

      /*
       * Function called once data is loaded from CSV.
       * Calls bubble chart function to display inside #vis div.
       */
      function display(error, data) {
        if (error) {
          console.log(error);
        }

        myBubbleChart('#vis', data);
      }

      /*
       * Sets up the layout buttons to allow for toggling between view modes.
       */
      function setupButtons() {
        d3.select('#toolbar')
          .selectAll('.button')
          .on('click', function() {
            // Remove active class from all buttons
            d3.selectAll('.button').classed('active', false);
            // Find the button just clicked
            var button = d3.select(this);

            // Set it as the active button
            button.classed('active', true);

            // Get the id of the button
            var buttonId = button.attr('id');

            // Toggle the bubble chart based on
            // the currently clicked button.
            myBubbleChart.toggleDisplay(buttonId);
          });
      }

      /*
       * Helper function to convert a number into a string
       * and add commas to it to improve presentation.
       */
      function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }

        return x1 + x2;
      }

      // Load the data.
      d3.json('data/data.json', display);

      // setup the buttons.
      setupButtons();
